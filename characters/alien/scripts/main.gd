extends CharacterBody2D

@onready var _alien = $AnimatedSprite2D
@export var speed = 200

func _process(_delta):
	if Input.is_action_pressed("ui_right"):
		_alien.flip_h = false
		_alien.play("walk")
	elif Input.is_action_pressed("ui_left"):
		_alien.flip_h = true
		_alien.play("walk")
	elif Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down"): 
		_alien.play("walk")
	else:
		_alien.stop()

func _physics_process(_delta):
	get_input()
	move_and_slide()
	
func get_input():
	var input_direction = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	velocity = input_direction * speed
